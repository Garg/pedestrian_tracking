/*! \file
@mainpage Multi-object Tracking
@author Author
- Name: Sourav Garg
- Affiliation: TCS Innovation Lab, New Delhi
- Email: garg.sourav@tcs.com, sourav.sahaji@gmail.com
- Date: Jun 24, 2015
*/

#include<multiTrack.h>

//#include "pedestrian_detection_multitracking.cpp"

#define DATASET                 0           // 0    input path to video in argument
// 1    ETH_bahnhof_seq_03
// 2    ETH_sunny_day
// 3    TUD_Stadtmitte
// 4    PETS09


#if(DATASET == 0)
#define DETECTION_RESPONSE      1            // 1 OpenCv HoG
#else
#define DETECTION_RESPONSE      2          // 2 Nevatia_USC_JRoG_XML      3 MOT_Challenge
#endif

#define HOG_THRESH              0.5         // OpenCv HoG Threshold for hit_rate
#define SKIP_FRAMES             0           // Skip these many initial frames

int main(int argc, char ** argv)
{
    cv::VideoCapture capInput;

    string outputPath = "./dataset_result/";
    char datasetString[2];
    sprintf(datasetString,"%d",DATASET);
    outputPath.append(datasetString);
    outputPath.append("/");

    string outTrackDocName = outputPath;

    // Corresponding to MOT Challenge Standards
    string outTrackDocName2 = outputPath;
    ofstream outDoc2;

    int totalFrames;
#if(DATASET == 0)
    if(argc != 2)
    {
        cerr << "Input video path not provided" << endl;
        exit(-1);
    }
    capInput.open(argv[1]);
    outTrackDocName.append("output.xml");
    outTrackDocName2.append("output.txt");
    totalFrames = 9999;
#elif(DATASET == 1)
    capInput.open("../input/video/ETH_bahnhof_seq_03.avi");
    outTrackDocName.append("ETH_bahnhof.avi.tracking.xml");
    outTrackDocName2.append("ETH-Bahnhof.txt");
    totalFrames = 999;
#elif(DATASET == 2)
    capInput.open("../input/video/ETH_sunny_day.avi");
    outTrackDocName.append("ETH_sunny_day.avi.tracking.xml");
    outTrackDocName2.append("ETH-Sunnyday.txt");
    totalFrames = 354;
#elif(DATASET == 3)
    capInput.open("../input/video/TUD_Stadtmitte.avi");
    outTrackDocName.append("TUD_Stadtmitte.avi.tracking.xml");
    outTrackDocName2.append("TUD-Stadtmitte.txt");
    totalFrames = 179;
#elif(DATASET == 4)
    capInput.open("../input/video/PETS09.avi");
    outTrackDocName.append("PETS09.avi.tracking.xml");
    outTrackDocName2.append("PETS09-S2L1.txt");
    totalFrames = 795;
#endif


    string outDirSysCall = "mkdir -p ";
    outDirSysCall.append(outputPath);
    int sysRetVal = system(outDirSysCall.c_str());
    outDoc2.open(outTrackDocName2.c_str());

    //----------------------------------------------------------------------------------
    // Reading human detection response from the file in XML format
    //----------------------------------------------------------------------------------

#if(DETECTION_RESPONSE == 2)

    vector< vector<Rect> > detROIs;

#if(DATASET == 0)
    cerr << "No detection input for this video" << endl;
    exit(-1);
    TiXmlDocument detectionFile;
#elif(DATASET == 1)
    TiXmlDocument detectionFile("../input/detection/bahnhof_raw.avi.detection.xml");
#elif(DATASET == 2)
    TiXmlDocument detectionFile("../input/detection/sunnyday_raw.avi.detection.xml");
#elif(DATASET == 3)
    TiXmlDocument detectionFile("../input/detection/TUD_Stadtmitte.avi.detection.xml");
#elif(DATASET == 4)
    TiXmlDocument detectionFile("../input/detection/PETS09_View001_S2_L1_000to794.avi.detection.xml");
#endif

    bool loadOK = detectionFile.LoadFile();

    if(!loadOK)
    {
        cout << "Detection Response XML file loading failed, exiting" << endl;
        exit(-1);
    }

    // Create a handle
    TiXmlHandle detHandle(&detectionFile);
    TiXmlElement* detElem;
    TiXmlHandle hroot(0);

    // Get root element
    detElem = detHandle.FirstChildElement().Element();
    if(!detElem)
    {
        cout << "No root element for XML" << endl;
        exit(-1);
    }
    else
    {
        string detName = detElem->Value();
        cout << "Reading XML File - " << detElem->Attribute("fname") << endl;

        int start_frame = atoi(detElem->Attribute("start_frame"));
        int end_frame = atoi(detElem->Attribute("end_frame"));

        cout << "Start Frame for Input - " << start_frame << endl;
        cout << "End Frame for Input - " << end_frame << endl;

        hroot = TiXmlHandle(detElem);
    }

    TiXmlElement* frameElement = hroot.FirstChild("Frame").Element();

    // Iterate over all frame elements
    for(frameElement; frameElement; frameElement = frameElement->NextSiblingElement())
    {
        int frameNumber = atoi(frameElement->Attribute("no"));

        TiXmlHandle frameListRoot = TiXmlHandle(frameElement);

        vector<Rect> det_roi;

        TiXmlElement* objListElem = frameListRoot.FirstChild("ObjectList").FirstChild().Element();

        // Iterate over all objects in the object list of a frame
        for(objListElem; objListElem; objListElem = objListElem->NextSiblingElement())
        {
            TiXmlHandle objectListRoot = TiXmlHandle(objListElem);
            TiXmlElement* objectElem = objectListRoot.FirstChild("Rect").Element();

            Rect tempROI;
            tempROI.height = atoi(objectElem->Attribute("height"));
            tempROI.width = atoi(objectElem->Attribute("width"));
            tempROI.x = atoi(objectElem->Attribute("x"));
            tempROI.y = atoi(objectElem->Attribute("y"));

            det_roi.push_back(tempROI);
        }

        detROIs.push_back(det_roi);
    }

    //----------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------


#elif(DETECTION_RESPONSE==3)
    ifstream detFile;
#if(DATASET==1)
    detFile.open("../../dataset2/MOT-challenge/2DMOT2015Labels/train/ETH-Bahnhof/det/det.txt");
#elif(DATASET==2)
    detFile.open("../../dataset2/MOT-challenge/2DMOT2015Labels/train/ETH-Sunnyday/det/det.txt");
#elif(DATASET==3)
    detFile.open("../../dataset2/MOT-challenge/2DMOT2015Labels/train/TUD-Stadtmitte/det/det.txt");
#elif(DATASET==4)
    detFile.open("../../dataset2/MOT-challenge/2DMOT2015Labels/train/PETS09-S2L1/det/det.txt");
#endif
    vector< vector<Rect> > detROIs;
    vector<Rect> detROIsPerFrame;
    int tempCounter=1;
    string oneLine;
    while(!detFile.eof())
    {
        float var[10];
        for(int i2=0; i2<10; i2++)
        {
            if(i2==9)
                getline(detFile,oneLine);
            else
                getline(detFile,oneLine,',');
            stringstream ss(oneLine);
            ss >> var[i2];
        }

        if(var[0]==tempCounter)
        {
            Rect detRoi(var[2],var[3],var[4],var[5]);
            detROIsPerFrame.push_back(detRoi);
        }
        else
        {
            detROIs.push_back(detROIsPerFrame);
            detROIsPerFrame.clear();
            Rect detRoi(var[2],var[3],var[4],var[5]);
            detROIsPerFrame.push_back(detRoi);
            tempCounter++;
        }
    }
#endif

    //Declaring File Pointers for writing timing
    FILE *fp_ind,*fp_runavg;
    ofstream fp_avg;

    string outFileName;
#if(TIMER_FILE)
    {
#if(RUN_AVG)
        outFileName = outputPath;
        fp_runavg = fopen(outFileName.append("./runavg_time.txt").c_str(),"w");
#endif
#if(IND_TIME)
        outFileName = outputPath;
        fp_ind = fopen(outFileName.append("./ind_time.txt").c_str(),"w");
#endif
        outFileName = outputPath;
        fp_avg.open(outFileName.append("./avg_time.txt").c_str());
    }
#endif

    float avgTime=0.0,runavg_hog=0.0,ind_hog=0.0;

    int avg_count =0;

    map<int,MultiTracker> Tracker;//Vector storing agents

    vector<Rect> HogWin;//Vector storing Hog Windows in each frame

    // Write the algo results in an XML using TinyXML
    //******
    TiXmlDocument doc;
    TiXmlDeclaration* decl = new TiXmlDeclaration("1.0","","");
    doc.LinkEndChild(decl);

    TiXmlElement* root = new TiXmlElement("Video");
    doc.LinkEndChild(root);
    root->SetAttribute("fname","ETH");
    root->SetAttribute("start_frame",0);
    root->SetAttribute("end_frame",totalFrames);
    //******

    Mat firstImg;

    for(int k1=0; k1<SKIP_FRAMES; k1++)
        capInput>> firstImg;

    capInput >> firstImg;

    if(firstImg.empty())
    {
        cout << "Empty First Image, exiting" << endl;
        exit(-1);
    }

    Mat frameData = firstImg.clone();
    Mat prevImg = firstImg.clone();

    int detRect = 0;
#if(DETECTION_RESPONSE == 1)

    Mat frameData2;
    float scaleFactor = 1;
    cv::resize(frameData,frameData2,Size(scaleFactor*frameData.cols,scaleFactor*frameData.rows));
    vector<Rect> HogWin2;
    //    int detRect2 = HogDetector(frameData2,HogWin2,0.5);

    detRect = HogDetector(frameData2, HogWin,HOG_THRESH);
    for(int i1=0; i1<HogWin.size(); i1++)
    {
        HogWin[i1].x /= scaleFactor;
        HogWin[i1].y /= scaleFactor;
        HogWin[i1].width /= scaleFactor;
        HogWin[i1].height /= scaleFactor;
    }

#elif(DETECTION_RESPONSE == 2)
    detRect = detROIs[0].size();
    if(detRect !=0)
        HogWin.assign(detROIs[0].begin(),detROIs[0].end());
#elif(DETECTION_RESPONSE == 3)
    detRect = detROIs[0].size();
    if(detRect !=0)
        HogWin.assign(detROIs[0].begin(),detROIs[0].end());
#endif

    filterDetections(firstImg,HogWin,true);

#if(SHOW_DETECTIONS)
    Mat detImageTempFirst = firstImg.clone();
    for(int k3=0; k3<HogWin.size(); k3++)
    {
        rectangle(detImageTempFirst,HogWin[k3],Scalar(0,255,0),2);
        char txt[20];
        sprintf(txt,"%d",k3);
        putText(detImageTempFirst,txt,HogWin[k3].tl(),2,1,Scalar(0,0,255),2);
    }
    namedWindow("firstFrameInitDetections",WINDOW_NORMAL);
    imshow("firstFrameInitDetections",detImageTempFirst);
    waitKey(WAITKEY);
#endif

#if(VIDEO_OUT)
    string outVidName = outputPath;
    outVidName.append("outTrack.avi");

    VideoWriter video3(outVidName, CV_FOURCC('D','I','V','3'), 10,Size(frameData.cols,frameData.rows),1);
#endif

    for(int count=0; count < HogWin.size() ; count++)
        checkBoundary(frameData,HogWin[count]);//Checking boundary for Hog Windows

    //Initializing of agents detected in first frame
    for(int i=0 ; i< HogWin.size() ; i++ )
    {
        Tracker[globalID].initAgent(frameData , HogWin[i], 0, HogWin);
        Tracker[globalID].id = globalID;
        globalID++;

    }

    // Display the first image
    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
        Tracker[i1->first].markAgents(frameData);

    map< pair<int,int> , KalmanFilter > relation2; // A Kalman Filter Predictor for every pair of tracker

    // Initialize the Bayesian Nodes Prior Probability
    Mat occ_prob(HogWin.size(),HogWin.size(),CV_32FC1,-1);  // Matrix contains the occlusion probability
    Mat dij_Mat(HogWin.size(),HogWin.size(),CV_32FC1,-1);
    vector<Point2f> nodeLoc;
    for(int i1=0; i1<HogWin.size(); i1++)
        nodeLoc.push_back(0.5*(HogWin[i1].tl()+HogWin[i1].br()));

    for(int i1=0; i1<nodeLoc.size(); i1++)
        for(int j1=i1+1; j1<nodeLoc.size(); j1++)
        {
            float dij = norm(nodeLoc[i1]-nodeLoc[j1]);
            float prior = 0.5;//1-dij/640;

            occ_prob.at<float>(i1,j1) = prior;
            occ_prob.at<float>(j1,i1) = prior;

            dij_Mat.at<float>(i1,j1) = dij;
            dij_Mat.at<float>(j1,i1) = dij;
        }

    updateNodeRelations(Tracker,relation2);

    string dataFileName = outputPath;
    dataFileName.append("dataFile.txt");

    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------
    // Iteration loop starts here
    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    Mat img = firstImg.clone();

    // Read frames in a loop
    int frameNum=-1+SKIP_FRAMES;
    while(1)
    {
        frameNum++;
        cout << "Frame Number - " << frameNum+1 << endl;

        double start_ =  getTickCount();//time.getElapsedTimeInMilliSec();

        avg_count++;

#if(TIMER_FILE)
#if(IND_TIME)
        fprintf(fp_runavg,"%d\t\t",frameNum+1);
#endif
#if(RUN_AVG)
        fprintf(fp_ind,"%d\t\t",frameNum+1);
#endif
        fp_avg << frameNum+1 << "\t\t";
#endif
        Mat tempImg = img.clone();

        // Displaying frame Number
        char tempText[100];
        sprintf(tempText,"%d",frameNum+1);
        putText(tempImg,tempText,Point(50,50),2, 1,Scalar(0,255,0),2,LINENAME,false);

        HogWin.clear();

        // Call HOG on the input frame
        double start =  getTickCount();//time.getElapsedTimeInMilliSec();

#if(DETECTION_RESPONSE == 1)

        Mat imgDet;
        cv::resize(img,imgDet,Size(scaleFactor*img.cols,scaleFactor*img.rows));

        detRect = HogDetector(imgDet, HogWin,HOG_THRESH);
        for(int i1=0; i1<HogWin.size(); i1++)
        {
            HogWin[i1].x /= scaleFactor;
            HogWin[i1].y /= scaleFactor;
            HogWin[i1].width /= scaleFactor;
            HogWin[i1].height /= scaleFactor;
        }


#elif(DETECTION_RESPONSE == 2)
        if(frameNum < detROIs.size())
        {
            detRect = detROIs[frameNum].size();
            HogWin.clear();
            for(int i1=0; i1<detROIs[frameNum].size(); i1++)
            {
                Rect r1 = detROIs[frameNum][i1];
                int areaInit = r1.area();
                checkBoundary(img,r1);
                int areaNow = r1.area();
                if(areaInit == areaNow)
                    HogWin.push_back(r1);
            }
            detRect = HogWin.size();
        }
        else
            detRect = 0;
#elif(DETECTION_RESPONSE==3)
        if(frameNum<detROIs.size())
        {
            detRect = detROIs[frameNum].size();
            if(detRect !=0)
                HogWin.assign(detROIs[frameNum].begin(),detROIs[frameNum].end());
        }
#endif

        detRect = HogWin.size();

        double end = getTickCount();//time.getElapsedTimeInMilliSec();

#if(SHOW_DETECTIONS)
        Mat detImageTemp = img.clone();
        for(int k3=0; k3<HogWin.size(); k3++)
        {
            rectangle(detImageTemp,HogWin[k3],Scalar(0,255,0),2);
            char txt[20];
            sprintf(txt,"%d",k3);
            putText(detImageTemp,txt,HogWin[k3].tl(),2,1,Scalar(0,0,255),2);
        }
        namedWindow("tempdet",WINDOW_NORMAL);
        imshow("tempdet",detImageTemp);
        waitKey(0);
#endif

#if(TIMER_FILE)
        {
#if(RUN_AVG)
            {
                runavg_hog = (runavg_hog + ((float)(end-start)/1000.0));
                fprintf(fp_runavg,"%f\t",runavg_hog/((float)avg_count));
            }
#endif
#if(IND_TIME)
            {
                ind_hog = ((float)(end-start)/1000.0);
                fprintf(fp_ind,"%f\t",ind_hog);
            }
#endif
        }
#endif
        //Label saying whether Hog Window is associated to some MS Window
        //associated: hogLabel[i] =1;
        int hogLabel[HogWin.size()];
        for(int i =0; i < HogWin.size(); i++)
            hogLabel[i]=0;
        for(int count=0; count < HogWin.size() ; count++)
            checkBoundary(frameData,HogWin[count]);


        track_TS3(img.clone(),Tracker,HogWin,outputPath,frameNum,root,relation2,outDoc2);

        double end_=  getTickCount();//time.getElapsedTimeInMilliSec();
        avgTime = avgTime + ((float)(end_-start_)/getTickFrequency());
#if(TIMER_FILE)
        fp_avg << avgTime/(float)(avg_count) << endl;
#endif

        // Display the results
        for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
            Tracker[i1->first].markAgents(tempImg);

#if(VIDEO_OUT)
        video3.write(tempImg);
#endif
        if(SHOW_IMAGE)
        {
            namedWindow("img",WINDOW_NORMAL);
            imshow("img1",tempImg);
           // waitKey(0);
        }

        char exitChar = waitKey(WAITKEY);
        if(exitChar == 'p' || exitChar == 'P')
            WAITKEY = WAITKEY == 1 ? 0 : 1;

        if(exitChar == '\x1b')
        {
            cout << "exiting" << endl;
            destroyWindow("img");
            break;
        }

        char frameNumTxt[20];
        sprintf(frameNumTxt,"%d",frameNum+1);

        capInput >> img;
        prevImg = img.clone();

        if(img.empty())
        {
            cout << "Empty Image read, exiting" << endl;
            break;
        }

        // In Nevatia_USC, they have skipped one frame somehow, so this is for levelling the equation
#if(DATASET == 1)
        if(frameNum == 998)
            break;
#endif
    }



    for(map<int,MultiTracker>::iterator i1=Tracker.begin(); i1!=Tracker.end(); i1++)
    {
        writeResultFile(Tracker[i1->first],root);
    }

    doc.SaveFile(outTrackDocName.c_str());

    cout << "done" << endl;
    if(TIMER_FILE)
    {
#if(RUN_AVG)
        fclose(fp_runavg);
#endif
#if(IND_TIME)
        fclose(fp_ind);
#endif
        fp_avg.close();
    }
#if(VIDEO_OUT)
    video3.release();
#endif
}
