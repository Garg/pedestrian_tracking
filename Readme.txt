Multi-object Tracking (Tested on Ubuntu 14.04)

1. Dependencies

	a) Libraries
		-- OpenCv (Tested with version 2.4.9)
		-- LibDAI (Tested with version 0.3.2)		
			## can be downloaded from https://staff.fnwi.uva.nl/j.m.mooij/libDAI/
		-- TinyXML 					
			## Ported with this package in /libs
			## can be downloaded from http://sourceforge.net/projects/tinyxml/

2. Build

	- cd <package_path>/build/
	- cmake ..
	- make

3. Run

	- cd <package_path>/bin/
	- ./humanTracking <video_path>
			
4. File Details
	- multiTrack.cpp: It contains the main function that calls the tracking routine to track multiple people in a video.
	- multiTrack.h: It contains various functions and class declaration for multiTracking.
	
5. Useful Macros
	- DATASET: To set the input dataset to be used. Hard-coded names of files for i/o are avaialble for four datasets. The default is 0
	  for reading input video from the path provided. For other values 1-4 no argument needs to be passed as input is read from /input/video/
	  **If the /input/video directory is empty, the same can be found from their respective websites or the links mentioned below. The image 
	  sequence has to be then converted into avi.
	- DETECTION_RESPONSE: This is to select the input detection response for running the detector on every frame. 1 is for OpenCv HoG detector,
	  2 is for Nevatia et. al. JRoG detector (http://iris.usc.edu/people/yangbo/downloads.html)
	  3 is for MOT Challenge Benchmarking which is standard HoG-SVM detector (http://motchallenge.net/)
	- Other macros are well explained in the code itself.
	
6. Ouput
	- Ouput is generated in /bin/dataset_result directory corresponding to each dataset.
	- The results are generated for both Nevatia et. al. XML format and MOT Challenge Benchmarking text format.
	- Video ouput, Timing file is generated if the corresponding macro is switched ON.
	- factor graph file factor_TS3.fg is generated and modified for processing each frame of video.
	
7. Evaluation
	- The evaluation tool we are using is Nevatia et. al. and is available in /Evaluation/ or can be downloaded from the web page
	  (http://iris.usc.edu/people/yangbo/downloads.html).
	  The executable can be run using "wine" on Ubuntu.
	- The other evaluation tool is MOT Benchmarking. Please refer to the web link for more information. 
